# Milestone 6 - jQuery and Bootstrap Libraries


#### Overview Section

- This is Milestone 6 which is focused on implementing jQuery to support showing and hiding content, and Bootstrap to create a navigation bar for the web page. 


#### References

- This is my current Sitemap for Topic 6. 
     
     
![Sitemap](<media/sitemap2.png>)

- These are my WireFrames for the main page as well as secondary pages.
    
     ![Wireframe 1](<media/mainWireframe2.png>)

     ![Wireframe 2](<media/secondaryWireframe2.png>)

- This is the page where I implemented the jQuery hide/show buttons."

-Show 
     ![Show More](media/t6m1.png)
-Hide
     ![Hide All](media/t6m2.png)

-This is the Bootstrap navigation bar that I included on every single page.    

![Navigation Bar](media/t6m3.png)

- This is my Recording link. 

     https://www.loom.com/share/103633334981433084c00bfb9a0c2b1a?sid=584d8a8f-6e95-47c9-9bd5-63fd6ae818fa

#### Conclusion

In Conclusion, I have learned how to implement jQuery into the creation of buttons on my website. Along with jQuery, I also learned the fundamentals of Bootstrap and how to create a navigation bar for a web page.

