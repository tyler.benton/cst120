# Milestone 5


#### Overview Section

- This is Milestone 5 which is focused on creating different types of event handles on a minimum of two pages. The JavaScript must also be able to be reused across pages.


#### References

- This is my current Sitemap for Topic 5. 
     
     
![Sitemap](<media/sitemap2.png>)

- These are my WireFrames for the main page as well as secondary pages.
    
     ![Wireframe 1](<media/mainWireframe2.png>)

     ![Wireframe 2](<media/secondaryWireframe2.png>)

- This is my page that I created buttons on"

-Before 
     ![Before Button](media/m51.png)
-After
     ![After Button](media/m52.png)

-This is my page where I created an "onmouseover" input that enlarges the image being hovered with a mouse over by the user.   

-Before
     ![Before Mouse Over](media/m53.png)
-After
     ![After Mouse Over](media/m54.png)

- This is my Recording link. 

     https://www.loom.com/share/3c5d177234da477c806a06e9cf81e2ca?sid=c2f4b2c1-39f1-4173-bd34-b43fd13bd6af

#### Conclusion

In Conclusion, I have learned how to implement event handles into my website with some knowledge of JavaScript. The events added to my website should make the page feel more interactive.

