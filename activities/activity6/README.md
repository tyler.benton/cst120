# Activity 6: jQuery and Bootstrap

## Front page

- Tyler Benton
- College of Humanities and Social Sciences, Grand Canyon University
- CST-120-O500
- Bobby Estey
- April 7, 2024

## Executive Summary

In this activity, you will learn the jQuery JavaScript framework

## Screenshots

- This is the first screenshot for this activity

![Screenshot 1](images/t6a1.png)

- This is the second screenshot for this activity.

![Screenshot 2](images/t6a2.png)

- This is the third screenshot for this activity.

![Screenshot 3](images/t6a3.png)

- This is the fourth screenshot for this activity.

![Screenshot 4](images/t6a4.png)
