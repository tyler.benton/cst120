# Activity 3: CSS

## Front page

- Tyler Benton
- College of Humanities and Social Sciences, Grand Canyon University
- CST-120-O500
- Bobby Estey
- March 24, 2024

## Executive Summary

In this activity, you will learn the basics of the JavaScript programming language.

## Screenshots

- This is the first screenshot for this activity

![Screenshot 1](media/T4A1.png)

- This is the second screenshot for this activity.

![Screenshot 2](media/T4A2.png)

- This is the SoloLearn screenshot.

![SoloLearn Screenshot](media/T4SL.png)


