# Activity 1: Tools Install and Basic HTML

## front page

- Tyler Benton
- College of Humanities and Social Sciences, Grand Canyon University
- CST-120-O500
- Bobby Estey
- March 10, 2024

## Executive Summary

This activity ...

## Screenshots

- this is the first screenshot

![Example1a](EX1.png)

- this is the second screenshot

![Example2a](EX2.png)

- this is the third screenshot

![Example3a](EX3.png)

- this is the fourth screenshot

![Example4a](EX4.png)

- this is the sololearn screenshot

![Sololearn](SL2.png)


|First Name|Last Name|
|--|--|
|Tyler|Benton|
|Bobby|Estey|