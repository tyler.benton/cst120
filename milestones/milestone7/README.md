# Milestone 7 - Final Website


#### Overview Section

- This is Milestone 7 which is focused on finishing our webpages and implementing Bootstrap form tags to support user data entry.


#### References

- This is my final Sitemap for Topic 7. 
     
     
![Sitemap](<media/sitemap2.png>)

- These are my WireFrames for the main page as well as basic idea for my secondary pages.
    
     ![Wireframe 1](<media/mainWireframe2.png>)

     ![Wireframe 2](<media/secondaryWireframe2.png>)

- This is my contact me page that I implemented Bootstrap to create a form.

     ![Contact Me Page](media/t7m1.png)

- After Submission of Form

     ![After Submission](media/t7m2.png)

- This is my Recording link. 

     https://www.loom.com/share/279c435dd9c34bffa5ce53502c853e96?sid=d910d84d-f62a-4424-82de-151f2c7f4562

#### Conclusion

In Conclusion, I have learned much about how to develope a webpage during my 7 weeks in this class. This week specifically, I learned how to create a contact form on a webpage.  
