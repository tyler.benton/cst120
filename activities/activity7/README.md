# Activity 7: HTML Forms Processing

## Front page

- Tyler Benton
- College of Humanities and Social Sciences, Grand Canyon University
- CST-120-O500
- Bobby Estey
- April 14, 2024

## Executive Summary

In this activity, you will learn some advanced web development technologies, including the use of Bootstrap Forms and how to support reusable page content.

## Screenshots

- This is the first screenshot for this activity

![Screenshot 1](images/t7a1.png)

- These are the second screenshots for this activity.

![First Screenshot 2](images/t7a2.png)![Second Screenshot 2](images/t7a2s.png)

- This is the third screenshot for this activity.

![Screenshot 3](images/t7a3.png)

- This is the fourth screenshot for this activity.

![Screenshot 4](images/t7a4.png)