# Milestone 2


#### Overview Section

- This is Milestone 2 which documents the requirements, development process, screenshots and recording for this week


#### References

- This is my Sitemap. 
     
     
![Sitemap](<media/sitemap2.png>)

- These are my wireframes for the main page as well as secondary pages.
    
     ![Wireframe 1](<media/mainWireframe2.png>)

     ![Wireframe 2](<media/secondaryWireframe2.png>)

- This is the About Me Page.

     ![Main About Me Page](media/mainPage.png)
     
- This is new Page 1. 

     ![Autobiography](media/autobiography.png)
     
- This is new Page 2. 

     ![My Career](media/myCareer.png)
     

- This is the Recording.
     - https://www.loom.com/share/2dffdc0d33ea49729bfb4fec58735b5c?sid=81d24b1b-23d1-4742-8034-4cb8006e8ac9

#### Conclusion

In Conclusion, I have set up two pages using a table on one and inserting a video for the other. I plan to add more information for each of the two pages. I also plan to create a video that will take the place of the temporary video that is placed there now. 

