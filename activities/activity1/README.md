# Activity 1: Tools Install and Basic HTML

## front page

- Tyler Benton
- College of Humanities and Social Sciences, Grand Canyon University
- CST-120-O500
- Bobby Estey
- March 3, 2024

## Executive Summary

This activity ...

## Screenshots

- this is the syllabus screenshot

![Syllabus](syllabus.png)

- this is the Topics and Objectives screenshot

![TopicsObjectives](topicsObjectives.png)

- this is the sololearn screenshot

![Sololearn](sololearn.png)

|First Name|Last Name|
|--|--|
|Tyler|Benton|
|Bobby|Estey|