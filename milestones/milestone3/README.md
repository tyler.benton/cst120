# Milestone 3


#### Overview Section

- This is Milestone 3 which is focused on CSS Styling for our webpages. 


#### References

- This is my Sitemap. 
     
     
![Sitemap](<media/sitemap2.png>)

- These are my wireframes for the main page as well as secondary pages.
    
     ![Wireframe 1](<media/mainWireframe2.png>)

     ![Wireframe 2](<media/secondaryWireframe2.png>)

- These are my pages that use CSS "colors, backgrounds, etc..."

     ![CSS Example 1](media/m31.png)

     ![CSS Example 2](media/m32.png)

     ![CSS Example 3](media/m33.png)
     
- This is my current use of an animation that is included on my main page.

     ![Animation Example](media/m34.png)
     

- This is the Recording.
     - https://www.loom.com/share/748c7762b471482e87fe9e1bc7b0d713?sid=261665f0-d26e-4c75-bb46-afd7c137530c

#### Conclusion

In Conclusion, I have learned about CSS and the many uses it has for styling. Specifically in my website I mostly used CSS to clean up old HTML formatting, to add animations, and to change font and background colors. 

