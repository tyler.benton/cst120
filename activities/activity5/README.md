# Activity 5: Advanced JavaScript

## Front page

- Tyler Benton
- College of Humanities and Social Sciences, Grand Canyon University
- CST-120-O500
- Bobby Estey
- March 31, 2024

## Executive Summary

In this activity, you will learn the advanced features of the JavaScript programming language.

## Screenshots

- This is the first screenshot for this activity

![Screenshot 1](media/a51.png)

- This is the second screenshot for this activity.

![Screenshot 2](media/a52.png)

- This is the third screenshot for this activity.

![Screenshot 2](media/a53.png)


- This is the SoloLearn screenshot.

![SoloLearn Screenshot](media/a5sl.png)


