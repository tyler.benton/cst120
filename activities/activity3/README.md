# Activity 3: CSS

## Front page

- Tyler Benton
- College of Humanities and Social Sciences, Grand Canyon University
- CST-120-O500
- Bobby Estey
- March 17, 2024

## Executive Summary

In this activity, you will continue building HTML pages styling the look and feel of the website using CSS.

## Screenshots

- this is the first screenshot

![ss1](sc/t31.png)

- this is the second screenshot

![ss2](sc/t32.png)

- this is the third screenshot

![ss3](sc/t33.png)

- this is the fourth screenshot

![ss4](sc/t34.png)

- this is the fifth screenshot

![ss5](sc/t35.png)

- this is the sixth screenshot

![ss6](sc/t36.png)

- this is the sololearn screenshot

![sssl](sc/t3s.png)


