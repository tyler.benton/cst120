# Milestone 4


#### Overview Section

- This is Milestone 4 which is focused on Cleanup and Enhancements.


#### References

- This is my current Sitemap. 
     
     
![Sitemap](<media/sitemap2.png>)

- These are my WireFrames for the main page as well as secondary pages.
    
     ![Wireframe 1](<media/mainWireframe2.png>)

     ![Wireframe 2](<media/secondaryWireframe2.png>)

- This is my page that I created a CSS FlexBox on"
   
![Photo Collection Page](media/m41.png)

- This is my Recording link. 

     https://www.loom.com/share/5ff284befd364fdcb99572636cebab66?sid=f2544319-6bcc-4c27-bd71-ef5b52d444ac

#### Conclusion

In Conclusion, I have created a FlexBox for my photo collection page on my webpage. Along with the creation of this FlexBox, I also cleaned up all of the code so that ti is neatly indented for better reading along with notes for what the code is doing. 

